import { readStringDelim } from 'https://deno.land/std@0.63.0/io/mod.ts';
import * as path from 'https://deno.land/std@0.63.0/path/mod.ts';
import {pathsToPoints, Point, intersects, intersection, segmentLength} from "./common/paths.ts";

const filename = path.join(Deno.cwd(), "input/three.txt");
let fileReader = await Deno.open(filename);

const grids: Array<Point[]> = [];

for await (let line of readStringDelim(fileReader, "\n"))  {
    const paths = line.split(',')
    if (paths.length > 1) {
        grids.push( pathsToPoints(paths) );
    }
}

function first() {
    console.log('closest intersection:');
    for (let gridIndex = 0; gridIndex <= grids.length - 1; gridIndex += 2) {

        let minDistance = Infinity;
        const a = grids[gridIndex];
        const b = grids[gridIndex + 1];

        for (let indexA = 0; indexA < a.length - 1; indexA++) {
            const aStart = a[indexA];
            const aEnd = a[indexA + 1];

            for (let indexB = 0; indexB < b.length - 1; indexB++) {
                const bStart = b[indexB];
                const bEnd = b[indexB + 1];
                const point = intersection(aStart,aEnd, bStart, bEnd) as Point;
                if (point) {
                    //console.log('line a: ', aStart, aEnd, ' line b: ', bStart, bEnd);
                    const dist = Math.abs(point.x) + Math.abs(point.y);
                    //console.log('point: ', point, ' dist: ', dist);
                    if (dist > 0 && dist < minDistance) {
                        minDistance = dist;
                    }
                }
            }
        }
        console.log('grid: ', gridIndex, ' intersection dist: ', minDistance);
    }
}

function second() {
    console.log('shortest path:');
    for (let gridIndex = 0; gridIndex <= grids.length - 1; gridIndex += 2) {

        let minDistance = Infinity;
        let pathLengthA = 0;
        let pathLengthB = 0;
        const a = grids[gridIndex];
        const b = grids[gridIndex + 1];

        for (let indexA = 0; indexA < a.length - 1; indexA++) {
            const aStart = a[indexA];
            const aEnd = a[indexA + 1];
            pathLengthB = 0;
            for (let indexB = 0; indexB < b.length - 1; indexB++) {
                const bStart = b[indexB];
                const bEnd = b[indexB + 1];

                const point = intersection(aStart,aEnd, bStart, bEnd) as Point;
                if (point) {
                    const toIntersectionLength = pathLengthA + pathLengthB + segmentLength(aStart, point) + segmentLength(bStart, point);
                    //console.log('line a: ', aStart, aEnd, ' line b: ', bStart, bEnd, 'toIntersectionLength: ', toIntersectionLength);
                    //console.log('point: ', point, ' dist: ', dist);
                    if (toIntersectionLength > 0 && toIntersectionLength < minDistance) {
                        minDistance = toIntersectionLength;
                    }
                }
                // add later
                pathLengthB += segmentLength(bStart, bEnd);
            }
            pathLengthA += segmentLength(aStart, aEnd);
        }
        console.log('grid: ', gridIndex, ' intersection dist: ', minDistance);
    }
}

first();
second();
