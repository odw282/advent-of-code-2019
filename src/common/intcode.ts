/**
 *
 * @param intCodes
 */
function processIntCodes(intCodes: Array<number>) {
    let operationIndex = 0;

    while(intCodes[operationIndex] !== 99) {

        const a = intCodes[intCodes[operationIndex+1]];
        const b = intCodes[intCodes[operationIndex+2]];

        switch(intCodes[operationIndex]) {
            case 1:
                intCodes[intCodes[operationIndex+3]] = a + b;
                break;
            case 2:
                intCodes[intCodes[operationIndex+3]] = a * b;
                break;

        }

        operationIndex += 4;
    }
}

export {
    processIntCodes
}
