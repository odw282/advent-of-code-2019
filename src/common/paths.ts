
interface Point {
    x: number
    y: number
}

function drawGrids(a: Point[], b: Point[]) {

    const xmina = a.reduce((min, point) => point.x < min ? point.x : min, 0);
    const xminb = b.reduce((min, point) => point.x < min ? point.x : min, 0);
    const xmin = Math.min(xmina, xminb);
    const ymina = a.reduce((min, point) => point.y < min ? point.y : min, 0);
    const yminb = b.reduce((min, point) => point.y < min ? point.y : min, 0);
    const ymin = Math.min(ymina, yminb);
    const xmaxa = a.reduce((max, point) => point.x > max ? point.x : max, 0);
    const xmaxb = b.reduce((max, point) => point.x > max ? point.x : max, 0);
    const xmax = Math.min(xmaxa, xmaxb);
    const ymaxa = a.reduce((max, point) => point.y > max ? point.y : max, 0);
    const ymaxb = b.reduce((max, point) => point.y > max ? point.y : max, 0);
    const ymax = Math.min(ymaxa, ymaxb);

    const grid: Array<string[]> = [];

    for(let y = ymax; y > ymin; y--) {
        grid[y] = [];
        for(let x = xmin; x < xmax; x++) {
            grid[y][x] = '.';
        }
    }
    grid[Math.abs(ymin)][Math.abs(xmin)] = 'O';

    for (let row of grid) {
        let line = "";
        for (let cell of row) {
            line += cell;
        }
        console.log(line);
    }

}

function pathsToPoints(paths: Array<string>) {
    let points: Point[] = [];
    let x = 0;
    let y = 0;

    points.push({
        x, y
    });

    for(let path of paths) {
        const dir = path[0];
        const dist = parseInt(path.slice(1));

        switch(dir) {
            case 'R':
                x += dist;
                break;
            case 'L':
                x -= dist;
                break;
            case 'U':
                y += dist;
                break;
            case 'D':
                y -= dist;
                break;
        }

        points.push({
            x, y
        })
    }

    return points;
}


function intersection(from1: Point, to1: Point, from2: Point, to2: Point): Point | false {
    const dX: number = to1.x - from1.x;
    const dY: number = to1.y - from1.y;

    const determinant: number = dX * (to2.y - from2.y) - (to2.x - from2.x) * dY;
    if (determinant === 0) return false; // parallel lines

    const lambda: number = ((to2.y - from2.y) * (to2.x - from1.x) + (from2.x - to2.x) * (to2.y - from1.y)) / determinant;
    const gamma: number = ((from1.y - to1.y) * (to2.x - from1.x) + dX * (to2.y - from1.y)) / determinant;

    // check if there is an intersection
    if (!(0 <= lambda && lambda <= 1) || !(0 <= gamma && gamma <= 1)) return false;

    return {
        x: from1.x + lambda * dX,
        y: from1.y + lambda * dY,
    };
}

function intersects(aStart: Point, aEnd: Point, bStart: Point, bEnd: Point) {
    let { x: a, y: b} = aStart;
    let { x: c, y: d} = aEnd;

    let { x: p, y: q} = bStart;
    let { x: r, y: s} = bEnd;
    let det, gamma, lambda;
    det = (c - a) * (s - q) - (r - p) * (d - b);
    if (det === 0) {
        return false;
    } else {
        lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
        gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
        return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }
};

function segmentLength(a: Point, b: Point) {
    return Math.abs(a.x - b.x + a.y - b.y);
}

export {
    Point,
    pathsToPoints,
    intersects,
    intersection,
    drawGrids,
    segmentLength
}
