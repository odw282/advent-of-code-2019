
const LOWER = 136760;
const UPPER = 595730;

const isSixDigits = (input: string) => input.length === 6
const isInRange = (input: string) => parseInt(input) >= LOWER && parseInt(input) <= UPPER;
const hasTwoAdjacentDigits = (input: string) => {
    let matches: { [key:string]: number } = {};
    for(let a = 0; a < input.length; a++) {
        matches[input[a]] = 0;
    }
    for(let a = 1; a < input.length; a++) {
        if (input[a] === input[a-1]) {
            matches[input[a]]++;
        }
    }
    for (let key in matches) {
        if (matches[key] === 1) {
            return true;
        }
    }
    return false;
}

const digitsNeverDecrease = (input: string) => {
    for(let a = 1; a < input.length; a++) {
        if (input[a] < input[a-1]) {
            return false
        }
    }
    return true;
}

function first() {

    let possiblePasswords = 0;

    for(let index = LOWER; index <= UPPER; index++) {
        const input = index.toString(10);
        if (!isSixDigits(input) || !isInRange(input) || !hasTwoAdjacentDigits(input) || !digitsNeverDecrease(input)) continue;
        possiblePasswords++;
    }

    console.log('first possiblePasswords: ', possiblePasswords, UPPER - LOWER);
}

first();
