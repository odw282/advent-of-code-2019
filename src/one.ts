import { readLines } from 'https://deno.land/std@0.63.0/io/mod.ts';
import * as path from 'https://deno.land/std@0.63.0/path/mod.ts';

const filename = path.join(Deno.cwd(), "input/one.txt");
let fileReader = await Deno.open(filename);


let total_fuel = 0;

function calculateFuelRequirement(mass: number) {
    const requirement = Math.floor(mass / 3) - 2;
    return requirement > 0 ? requirement : 0;
}

for await (let line of readLines(fileReader)) {
    const mass = parseInt(line);
    if (mass > 0) {
        let remainingMass = mass;
        while (calculateFuelRequirement(remainingMass) > 0) {
            remainingMass = calculateFuelRequirement(remainingMass);
            total_fuel += remainingMass;
        }
    }
}

console.log(total_fuel);
