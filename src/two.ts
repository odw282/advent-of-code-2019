import { readStringDelim } from 'https://deno.land/std@0.63.0/io/mod.ts';
import * as path from 'https://deno.land/std@0.63.0/path/mod.ts';
import { processIntCodes } from "./common/intcode.ts";

const filename = path.join(Deno.cwd(), "input/two.txt");
let fileReader = await Deno.open(filename);

const intCodes: Array<number> = [];

for await (let line of readStringDelim(fileReader, ","))  {
    const intCode = parseInt(line);
    if (intCode >= 0) {
        intCodes.push(intCode);
    }
}

function first(intCodes: Array<number>) {
    intCodes[1] = 12;
    intCodes[2] = 2;
    processIntCodes(intCodes);
    console.log('first:', intCodes[0]);
}

function second(intCodes: Array<number>) {

    const MAX = 99;
    const target = 19690720;
    let fuelRequirement = 0;
    let verb = 0;

    for(let noun = 0; noun <= MAX; noun++) {
        for(let verb = 0; verb <= MAX; verb++) {

            // create new copy of intCodes
            const instructions = [...intCodes];

            // set instructions
            instructions[1] = noun;
            instructions[2] = verb;

            // get new fuel requirements
            processIntCodes(instructions);

            // set calculated fuel requirements
            if (instructions[0] === target) {
                console.log('second:', noun, verb, 100 * noun + verb);
                return;
            }
        }
    }
}

first([...intCodes]);
second([...intCodes]);



